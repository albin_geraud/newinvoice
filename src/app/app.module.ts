import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PageheaderComponent } from './components/pageheader/pageheader.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CurrentinvoiceComponent } from './components/currentinvoice/currentinvoice.component';
import { BillpostComponent } from './components/currentinvoice/billpost/billpost.component';
import { ButtonsComponent } from './components/sidebar/buttons/buttons.component';


@NgModule({
  declarations: [
    AppComponent,
    PageheaderComponent,
    SidebarComponent,
    CurrentinvoiceComponent,
    BillpostComponent,
    ButtonsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
