export class Lineitems {

  name: String;
  description: String;
  quantity: number;
  price_cents: number;
  position: number;
  summe: number;

  constructor() {
  this.name = '';
  this.quantity = 0;
  this.description = '';
  this.price_cents = 0;
  this.position = 1;
  this.summe = 0;
  }
}
